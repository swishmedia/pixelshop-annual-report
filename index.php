<!DOCTYPE html>
<html>

<head>
	<title>Annual Report 2014-2015</title>
	<link rel="stylesheet" type="text/css" href="/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/css/styles.css">
	<link rel="stylesheet" type="text/css" href="/css/jquery.fullPage.css">
	<link rel="stylesheet" type="text/css" href="/css/magnific.css">
	<link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
	<script src="https://use.typekit.net/pbu5ywz.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>	
</head>

<body>
	<!-- header / sticky nav -->
	<div id="header">
		<img src="/i/logo.png" alt="logo" />
		<h1>Annual Report 2014-2015</h1>
		<ul id="nav">
			<li>
				<a href="#">Table of Contents</a>
				<ul id="fullPageMenu">
					<li><a href="#row2">Welcome to Our Annual Report</a></li>
					<li><a href="#row3">Thanks for Stopping By</a></li>
					<li><a href="#row4">The Future is Bright</a></li>
					<li><a href="#row5">We See Possibility Everywhere</a></li>
					<li><a href="#row6">We Are Collaborators</a></li>
					<li><a href="#row7">We Believe in The Power of People</a></li>
					<li><a href="#row8">Janet Holder</a></li>
					<li><a href="#row9">Board Members</a></li>
					<li><a href="#row10">Thank You</a></li>
				</ul>
			</li>
		</ul>	
	</div>
	<!-- sections start -->
	<div id="fullPage">
		<div class="section row1">
			<div class="content">
				<h2>Our Journey. Our Destination. Our Future.</h2>
				<div class="scrollDown"></div>
			</div>
		</div>
		<div class="section row2">
			<div class="content">
				<h2>Welcome to our Annual Report</h2>
				<div class="twoColWrap">
					<div>
						<img src="/i/chair.png" alt="Chair" />
						<p class="name">Noreen Taylor</p>
						<p class="title">Board Chair, Saint Elizabeth</p>
					</div>
					<div>
						<p>It is a pleasure and privilege to serve as the Chairman of the Board of Saint Elizabeth.  The work that is being done by all of you and the scope and importance of your participation is inspiring.  In every meeting we have, every member of the Board of Directors is impressed by the quality of work that is the hallmark of Saint Elizabeth.  </p>
						<p>On behalf of the Board, thank you for another rewarding year.  As a board we cannot wait to meet the challenges and opportunities of the coming year.  I hope that all of you share our enthusiasm.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="section row3">
			<div class="content">
				<h2>Thanks for Stopping By</h2>
				<div class="vidWrapper">
					<div class="vidContainer">
						<iframe src="https://www.youtube.com/embed/3M_vKnfbB6w" frameborder="0" allowfullscreen class="theVideo"></iframe>					
					</div>
				</div>
			</div>
		</div>
		<div class="section row4">
			<div class="content">
				<h2>The Future is Bright</h2>
				<div class="boardWrap">
					<div>
						<p>We are proud of where we have been, but brimming with excitement about where we are going. Here is a sneak peek at what we're working on in 2015-16:</p>
					</div>
					<ul class="rowSlider">
						<li><img src="/i/slide-elizz.jpg" title="Introducing Elizz, a new brand dedicated to All Things Caregiving" /></li>
						<li><img src="/i/SEHC-Medical-Centres.jpg" title="Launching Saint Elizabeth Medical Centres, offering integrated care to support healthy living" /></li>
						<li><img src="/i/Residential-Hospice.jpg" title="Investing over $1 million towards residential hospice and improving end of life care" /></li>
					</ul>					
				</div>
			</div>
		</div>		
		<div class="section row5">
			<div class="content">
				<h2>We See Possibility Everywhere</h2>
				<div class="circleWrap">
					<div class="circle1 group1" href="#inline1">
						<p>Powering the future of homecare</p>
						<img src="/i/row4circle1.png" alt="" />
					</div>
					<div class="circle2 group1" href="#inline2">
						<p>Sharing knowledge;<br> building expertise</p>
						<img src="/i/row4circle2.png" alt="" />
					</div>
					<div class="circle3 group1" href="#inline3">
						<p>Bringing knowledge <br>to life</p>
						<img src="/i/row4circle3.png" alt="" />
					</div>
				</div>
				<div class="factoid">
					<p>107 years of forward thinking</p>
				</div>
				<div class="factoid2">
					<p>1,350+ ideas shared through our employee SoapBox</p>
				</div>								
			</div>
		</div>
		<div class="section row6">
			<div class="content">
				<h2>We Are Collaborators</h2>
				<div class="circleWrap">
					<div class="circle1 group2" href="#inline4">
						<p>Integrating Care</p>
						<img src="/i/row5circle1.png" alt="" />
					</div>
					<div class="circle2 group2" href="#inline5">
						<p>St. Elizabeth<br> Health Career College</p>
						<img src="/i/row5circle2.png" alt="" />
					</div>
					<div class="circle3 group2" href="#inline6">
						<p>Sharing our story</p>
						<img src="/i/row5circle3.png" alt="" />
					</div>
				</div>
				<div class="factoid2 factRight">
					<p>First Canadian member of the Innovation Learning Network</p>
				</div>											
			</div>
		</div>
		<div class="section row7">
			<div class="content">
				<h2>We Believe in the Power of People</h2>
				<div class="circleWrap">
					<div class="circle1 group3" href="#inline7">
						<p>Igniting a movement</p>
						<img src="/i/row6circle1.png" alt="" />
					</div>
					<div class="circle2 group3" href="#inline8">
						<p>Celebrating 15 years of partnership</p>
						<img src="/i/row6circle2.png" alt="" />
					</div>
					<div class="circle3 group3" href="#inline9">
						<p>Shifting the culture<br>toward person and family centred care</p>
						<img src="/i/row6circle3.png" alt="" />
					</div>
				</div>
				<div class="factoid">
					<p>18,000 people visited everyday</p>
				</div>								
			</div>
		</div>
		<div class="section row8">
			<div class="content">
				<h2>Janet Holder</h2>
				<div class="boardWrap">
					<img src="/i/jan-holder.png" alt="" />
					<p>As we were finalizing our annual report, we heard the sad news our long-time friend and colleague, Janet Holder, lost her battle with cancer.  Janet's association with Saint Elizabeth spanned over 15 years - she served on both boards in an executive capacity.  Janet's commitment and wisdom helped guide our organizational journey and she will be missed.</p>
					<p><em>Photo - L to R Michael Decter, Noreen Taylor, Janet Holder, Shirlee Sharkey, and The Most Honourable Paul Martin</em></p>
				</div>
			</div>
		</div>
		<div class="section row9">
			<div class="content">
				<h2>Board Members</h2>
				<div class="boardWrap">
					<div>
						<p class="name">Noreen Taylor</p>
						<p class="title">Board Chair</p>
						<p class="history">Founder, The Charles Taylor Foundation</p>
						
						<p class="name">Heather McClure</p>
						<p class="title">Treasurer</p>
						<p class="history">Senior Vice President Operational Finance, Rogers Communications Inc.</p>

						<p class="name">Janet Holder</p>
						<p class="title">Secretary</p>
						<p class="history">Retired, Executive Vice President, Western Access, Enbridge Inc.</p>

						<p class="name">Bill Chambers</p>
						<p class="title">Vice-President</p>
						<p class="history">Brand, Communications and Corporate Affairs, CBC/Radio-Canada</p>

						<p class="name">Michael Fullan</p>
						<p class="title">Executive Director</p>
						<p class="history">Catholic Charities of the Archdiocese of Toronto</p>
					</div>
					<div>
						<p class="name">Don McCutchan</p>
						<p class="history">International Policy Advisor Gowlings</p>

						<p class="name">Shirlee Sharkey</p>
						<p class="history">President & CEO, Saint Elizabeth</p>

						<p class="name">Philip Smith</p>
						<p class="history">Senior Vice President, Chief Financial Officer & Head of Operations, Global Banking and Markets, Scotiabank</p>

						<p class="name">John Stackhouse</p>
						<p class="history">Senior Vice-President, Office of the CEO, RBC</p>
					</div>
				</div>
			</div>
		</div>
		<div class="section row10">
			<div class="content">
				<h2>Thank You</h2>
				<div class="boardWrap">
					<p>Because of the generous financial contributions of our Saint Elizabeth Foundation donors, we are able to support people, communities and families across Canada.</p>					
				</div>
			</div>
		</div>


	</div>
	<!-- sections end -->
	<!-- lightbox example -->
	<div class="possibility">
		<div id="inline1" class="inline_box mfp-hide">
			<img src="/i/Powering-the-future-of-home-care.jpg" alt="" />
			<a class="modalClose" href="#"><img src="/i/lightboxclose.jpg" alt="" /></a>
			<div class="modalContent">
				<h1>Powering the Future of Homecare</h1>
				<p>All around us, the rise of mobile technology is opening the door to new possibilities in how we work, play and connect - and health care is no exception. From remote patient monitoring to virtual visits to the power of big data, technology is fuelling transformative models of care. This year, Saint Elizabeth announced a landmark partnership with Samsung Canada to equip more than 5,000 of our remote workers with cutting-edge mobile technology, including GALAXY Tab S LTE connected tablets.  The initiative marked Samsung's biggest mobile enterprise partnership in Canada to date.</p>
				<p>Tablets and smartphones are providing Saint Elizabeth with a powerful and secure mobile solution to continue our business focus on excellence and innovation. Staff are using the devices and a range of apps to connect the circle of care in new ways, from scheduling appointments to more efficiently planning and navigating routes to electronically recording patient data. Building on our culture of innovation, the new platform is also helping us to unleash the power of virtual health, mobile health and online communities. <a href="#">Here's a glimpse of how we are leading the charge</a>.</p>
			</div>		
		</div>
		<!-- end lightbox example -->
		<div id="inline2" class="inline_box mfp-hide">
			<img src="/i/Sharing-knowledge.jpg" alt="" />
			<a class="modalClose" href="#"><img src="/i/lightboxclose.jpg" alt="" /></a>
			<div class="modalContent">
				<h1>Building strength; sharing expertise</h1>
				<p>It's in our nature as explorers to monitor the horizon for opportunities to spread hope and happiness!  This year, we proudly partnered with Long Term Care homes in Edmonton and Prince Edward County to provide leadership and direct care support. We also expanded our services to a new province, Nova Scotia. Sharing our expertise has allowed us to leverage our strengths and impact care and excellence within a brand new group of clients and communities.</p>
				<p>We understand that for smaller not-for-profits, handling specialized day to day operations like HR, IT and Finance can be a challenge. To help alleviate some of the pressure points for these vital community-based groups, we've partnered with a variety of them and have assumed responsibilities for these functions.  This approach allows these organizations to keep their focus on the crucial needs of their clients, and allows us to impact the broader ecosystem of community support.</p>
			</div>		
		</div>
		<!-- end lightbox example -->
		<div id="inline3" class="inline_box mfp-hide">
			<img src="/i/Bringing-knowledge-to-life.jpg" alt="" />
			<a class="modalClose" href="#"><img src="/i/lightboxclose.jpg" alt="" /></a>
			<div class="modalContent">
				<h1>Bringing knowledge to life</h1>
				<p>It's important to us to provide the best possible care to our clients - care that has been shown to be effective.  That's why Saint Elizabeth has made a strategic commitment to research - $10 million over 10 years - to design and improve care.</p>
				<p>The Saint Elizabeth Research Centre has been working with family caregivers to learn how best to support them. Family caregivers told us they need proactive supports and resources to help them to continue to provide care. Based on what we learned, we developed an <a href="https://www.saintelizabeth.com/Caregiver-Database/Home.aspx" target="_blank">online resource</a> for caregiver programs.  Then, we helped revise and expand Saint Elizabeth's caregiver support program to make sure it was meeting the needs of caregivers.  And, we are helping other caregiver programs, such as the Holland Bloorview Kids Rehabilitation Hospital, to re-design theirs! </p>
				<p>Visit the <a href="https://www.saintelizabeth.com/Services-and-Programs/Research-Centre.aspx" target="_blank">Saint Elizabeth Research Centre website</a> to learn more about our research.</p>
			</div>		
		</div>
	</div>
	<!-- end lightbox example -->	
	
	<!-- lightbox example -->
	<div class="collaborators">
		<div id="inline4" class="inline_box mfp-hide">
			<img src="/i/Integrating-care.jpg" alt="" />
			<a class="modalClose" href="#"><img src="/i/lightboxclose.jpg" alt="" /></a>
			<div class="modalContent">
				<h1>Advancing systems of change</h1>
				<p>Saint Elizabeth is excited to be part of a movement that is taking health care to the next level, paving the way for smoother transitions between hospital and home, and improved health outcomes. Building on our unique clinical and technology-supported programs that help keep people living at home as long as possible, we established several new partnerships with hospitals to develop and test innovative approaches that integrate funding and care, from cardiac surgery to congestive heart failure and chronic obstructive pulmonary disease.</p>
				<p>The goal of these projects is to improve the care experience for people and families as they journey through the health system, while achieving better quality and value for money. By supporting patients to return home safely and sooner following a hospital stay, we can help them get back to their everyday lives. What's more, this helps to free up acute care beds and prevent unnecessary hospital readmissions and visits to the emergency department.</p>
				<blockquote>
					<p>"The opportunity to work collaboratively across sectors allows us to learn from each other and be smarter and more creative in our approach."</p>
				</blockquote>
			</div>		
		</div>
		<!-- end lightbox example -->
		<div id="inline5" class="inline_box mfp-hide">
			<img src="/i/Saint-Elizabeth-Health-Career-College.jpg" alt="" />
			<a class="modalClose" href="#"><img src="/i/lightboxclose.jpg" alt="" /></a>
			<div class="modalContent">
				<h1>Saint Elizabeth Health Career College</h1>
				<p>Our exploring nature has led us to travel upstream to positively influence the health system - there is no better place to start than with the health care work force of the future.</p>
				<p>Building on our experience in Vancouver, we recently launched a network of innovative career colleges offering accredited diploma programs for personal support workers and health care assistants. Campuses are located in British Columbia and Ontario. Our curriculum equips graduates to deliver compassionate care based on the latest evidence and technological advancements. We've included incentives and connected with various community leaders to amplify the impact of these new graduates.</p>
				<p>Leveraging over a century of wisdom for greater social purpose and impact, the schools create exciting new opportunities for Saint Elizabeth to support communities and help address healthcare human resource constraints.</p>
				<blockquote>
					<p>"Investing in the future of health care - one graduate at a time..."</p>
				</blockquote>
			</div>		
		</div>
		<!-- end lightbox example -->
		<div id="inline6" class="inline_box mfp-hide">
			<img src="/i/Sharing-our-story.jpg" alt="" />
			<a class="modalClose" href="#"><img src="/i/lightboxclose.jpg" alt="" /></a>
			<div class="modalContent">
				<h1>Sharing our story</h1>
				<p>Did you know Saint Elizabeth was named after someone who was born over eight centuries ago? In 2014, we set out to tell our story in a fun and creative way. Working with an animation studio, we created <a href="https://www.youtube.com/watch?v=0-mzTEyfBC4" target="_blank">a cartoon starring our namesake Elizabeth</a>, a true trailblazer who spreads hope and happiness well beyond health care.</p>
				<p>This year we also had the opportunity to share our story - and the stage - with many people we admire. From stellar start-ups to leadership legends to Facebook friends, the exchange of wisdom and perspectives is something we just can't get enough of. Here's a look at some of the opportunities and connections that tickled our fancy in 2014-15.</p>
				<h2>Speaking engagements</h2>
				<div class="logoWrap">
					<img src="/i/logo-socap.jpg" alt="" />
					<img src="/i/logo-discovery.jpg" alt="" />
					<img src="/i/logo-nhlc.jpg" alt="" />
					<img src="/i/logo-mars.jpg" alt="" />
					<img src="/i/logo-appsh.jpg" alt="" />
					<img src="/i/logo-mohawk.jpg" alt="" />					
				</div>
				<h2>Media coverage</h2>
				<div class="logoWrap">
					<img src="/i/logo-globe.jpg" alt="" />
					<img src="/i/logo-bt.jpg" alt="" />
					<img src="/i/logo-cbc.jpg" alt="" />
					<img src="/i/logo-morning.jpg" alt="" />
					<img src="/i/logo-corus.jpg" alt="" />
					<img src="/i/logo-hospital.jpg" alt="" />					
				</div>
				<h2>Online connections</h2>
				<div class="logoWrap">
					<div>
						<img src="/i/logo-followers.jpg" alt="" />
						<p><strong>New Followers:</strong></p>
						<p>3,608</p>
					</div>
					<div>
						<img src="/i/logo-eng.jpg" alt="" />
						<p><strong>Engagements:</strong></p>
						<p>41,887</p>
					</div>
					<div>
						<img src="/i/logo-imp.jpg" alt="" />
						<p><strong>Impressions:</strong></p>
						<p>9,162,979</p>
					</div>
					<div>
						<img src="/i/logo-views.jpg" alt="" />
						<p><strong>Website pageviews (cumalative):</strong></p>
						<p>2,349,825</p>

					</div>
				</div>
			</div>		
		</div>
	</div>
	<!-- end lightbox example -->
	
	<!-- lightbox example -->
	<div class="people-power">
		<div id="inline7" class="inline_box mfp-hide">
			<img src="/i/Igniting-a-Movement.jpg" alt="" />
			<a class="modalClose" href="#"><img src="/i/lightboxclose.jpg" alt="" /></a>
			<div class="modalContent">
				<h1>Igniting a movement</h1>
				<p>At Saint Elizabeth, we visit 18,000 people in communities across Canada every day. Through acts of care and kindness, we strive to spread hope and happiness wherever we go. This year, we launched a 'Hope and Happiness' movement to recognize our special and unique culture, and to inspire others.  The #HopeAndHappiness campaign took off on social media, reaching over 316,000 people around the globe. Within hours, stories were flooding in from trailblazers across the country. To further amplify the movement, we gave each of our employees $25 to 'pay it forward' with gestures of kindness.</p>
				<p>Today, the Saint Elizabeth team is spreading hope and happiness in many ways - from buying coffee to bringing someone a birthday card. One service coordinator teamed up with her supervisor to create 'chemo relief kits' with comfort items for cancer patients, while a personal support worker purchased sewing supplies to assist clients with basic mending projects. These inspiring stories illustrate how simple acts of kindness can have a profound and lasting impact.</p>
				<p>In 2015, our Hope and Happiness movement was recognized with the 'Celebrating the Human Spirit Award' from the Canadian College of Health Leaders.</p>
				<blockquote>
					<p>"The initiative gives me Hope for our health care system, and Happiness in seeing the amazement people express."</p>
					<cite>Lynne, Personal Support Worker</cite>
				</blockquote>
			</div>		
		</div>
		<!-- end lightbox example -->
		<div id="inline8" class="inline_box mfp-hide">
			<img src="/i/Celebrating-15-years-of-partnership.jpg" alt="" />
			<a class="modalClose" href="#"><img src="/i/lightboxclose.jpg" alt="" /></a>
			<div class="modalContent">
				<h1>Celebrating 15 years of partnership</h1>
				<p>People living in First Nation communities face many challenges and barriers in accessing health care services. To help advance community-led approaches to health and wellbeing, Saint Elizabeth partners with First Nations, Inuit and Métis communities and organizations to support continuing education and quality care closer to home.</p>
				<p>The First Nations, Inuit and M&eacute;tis program provides virtual education at no cost to health care providers working in First Nation communities. Initiated in 2000 as a grassroots partnership, this unique program is active in all provinces and territories - with more than 500 indigenous communities and organizations participating.</p>
				<p>Our innovative knowledge sharing network also features webinars, community forums and 24/7 access to peers and experts. All content is developed in full partnership with First Nations, Inuit and Métis communities to ensure it is relevant and culturally rich. To date, over 2,000 health care providers have completed the online courses offered by Saint Elizabeth, totaling more than 4,000 hours of learning annually.</p>
				<blockquote>
					<p>"The Saint Elizabeth program has helped me to support the people and communities I work with in many ways, from working with Elders and their families, promoting self-care and building relationships." </p>
					<cite>Lyndsey Rhea, Aboriginal Patient Liaison Worker, Northern Health, BC</cite>
				</blockquote>
			</div>		
		</div>
		<!-- end lightbox example -->
		<div id="inline9" class="inline_box mfp-hide">
			<img src="/i/Shifting-the-culture.jpg" alt="" />
			<a class="modalClose" href="#"><img src="/i/lightboxclose.jpg" alt="" /></a>
			<div class="modalContent">
				<h1>Putting people and families first</h1>
				<p>We know that the needs of our clients and families must be a priority!  Our research found that this leads to better care, better relationships, and happier clients, families, and providers.</p>
				<p>We know that healthcare providers want to help clients meet their healthcare goals in a way that suits them and their families best, but often there are organizational barriers to doing so.</p>
				<p>We wanted to help shift the culture across organizations to ensure that providers are fully supported in providing PFCC.</p>
				<p>So, we developed a series of educational workshops and resources, based on our research, to help leaders, providers, and support-staff understand the principles of Person- and Family-Centred Care (PFCC) and implement PFCC practices across their organization.</p>
				<p>We piloted these workshops in 7 sites across Canada. People are finding out about the work we are doing. Now we are getting requests from various organizations to provide our PFCC expertise to them!</p>
				<p>So, we are launching <a href="https://www.saintelizabeth.com/Services-and-Programs/Research-Centre/Person-and-Family-Centred-Care-Institute.aspx" target="_blank">the Saint Elizabeth PFCC Institute</a>. Our Institute will help organizations shift the culture toward collaborating with clients and their families in designing, improving, and providing care.  Our innovative approach and expertise in this area is sure to help others. We hope to spread PFCC approaches across Canada!</p>
				<blockquote>
					<p>"Before, I thought of it as providing a service for the client. During the PFCC workshops, I learned that I can work as a partner with clients... [The workshops] reminded me to always think about the client and always communicate with the client so that we can understand each other."</p>
					<cite>- PSW, following PFCC workshop</cite>
				</blockquote>
			</div>		
		</div>
	</div>
	<!-- end lightbox example -->

</body>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/jquery.fullPage.min.js"></script>
<script type="text/javascript" src="js/magnific.min.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="js/func.js"></script>
</html>