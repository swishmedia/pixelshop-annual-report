//On Load
var possibilities;
var collaborators;
var peoplePower;

$(function(){
	//initiate full page js
	function buildFullPage(){
		$('#fullPage').fullpage({
			anchors: ['row1', 'row2', 'row3', 'row4', 'row5', 'row6', 'row7', 'row8', 'row9','row10'],
			menu: '#fullPageMenu',
			navigation: true,
			navigationPosition: 'right',
			touchSensitivity: 20,
			afterLoad: function(anchorLink, index){
		        $('.factoid, .factoid2').addClass('factoid-display');
		        
	    	},
			onLeave: function(index, nextIndex, direction){
				$('.factoid, .factoid2').removeClass('factoid-display');	
			}    			
		});
	}
	buildFullPage();
	
	//initiate scrollDown button
	$(document).on('click', '.scrollDown', function(){
	  $.fn.fullpage.moveSectionDown();
	});
	
	//possibility colorbox
	possibilities = $('.group1').magnificPopup({
		type:'inline',
		gallery:{enabled: true},
		callbacks:{
			open: function(){
				$.fn.fullpage.setAllowScrolling(false);
			},
			close: function(){
				$.fn.fullpage.setAllowScrolling(true);				
			}
		}
	});
	
	collaborators = $('.group2').magnificPopup({
		type:'inline',
		gallery:{enabled: true},
		callbacks:{
			open: function(){
				$.fn.fullpage.setAllowScrolling(false);
			},
			close: function(){
				$.fn.fullpage.setAllowScrolling(true);
			}
		}		
	});
	
	peoplePower = $('.group3').magnificPopup({
		type:'inline',
		gallery:{enabled: true},
		callbacks:{
			open: function(){
				$.fn.fullpage.setAllowScrolling(false);
			},
			close: function(){
				$.fn.fullpage.setAllowScrolling(true);
			}
		}		
	});
	
	$(".inline_box .modalClose").click(function(){
		magnificPopup.close();
		//$('#fullPage').css('position', 'relative');
	});
	
	//table of contents drop down
	$("#nav li").click(function(e){
		$(this).find('ul').toggle();
		var activeRow = $(this).find('a');
		if(activeRow.hasClass('activerow')){
			activeRow.removeClass('activerow');
		} else {
			activeRow.addClass('activerow');
		}
	});
	//initiate slider
	$('.rowSlider').bxSlider({
		captions: true,
		slideWidth: 782
	});
});